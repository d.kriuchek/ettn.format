from lxml import etree
from lxml.etree import tostring,canonicalize
from itertools import chain

parser = etree.XMLParser(remove_blank_text=True, encoding='UTF-8')
xml = etree.parse(rb'ettn_generic-recipient_signed.xml', parser=parser)
print(canonicalize(etree.tostring(xml.getroot().find('SIGN_ENVELOPE'), encoding='unicode', pretty_print=False)))
